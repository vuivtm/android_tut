package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        loadSNS();
    }
    /*
     * be call when tab load sns page
     * */
    public void loadSNS(){
        WebView webView;
//        String url = "https://sb.vtmlab.com/auth/loginstaff/demo";
        String url = "https://www.google.com/";
        webView = findViewById(R.id.webview);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(url);
    }
}
