package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        /*
        * get content from main
        * */
        Intent intent = getIntent();
        String msg = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String to = intent.getStringExtra(MainActivity.EXTRA_TO);
        /*
        * set data to: to, msg
        * */
        TextView msgView = findViewById(R.id.idDspMsg);
        TextView toView = findViewById(R.id.idDspTo);
        msgView.setText(msg);
        toView.setText(to);
    }
}
