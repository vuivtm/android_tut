package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.myapplication.message";
    public static final String EXTRA_TO = "com.example.myapplication.to";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        callWebView();
    }
    /*
    * be call when user tap btn send
    */
    public void sendMsg(View view){
        //do some thing here
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = findViewById(R.id.txtEditText);
        EditText toStr = findViewById(R.id.txtTo);
        String msg = editText.getText().toString();
        String to = toStr.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, msg);
        intent.putExtra(EXTRA_TO, to);
        startActivity(intent);
    }
    /*
    * call webview
    * */
    public void callWebView(View view){
        Intent intent = new Intent(this, WebviewActivity.class);
        startActivity(intent);
    }
    /*
    * call relative layout
    * */
    public void toRelativeLayout(View view){
        Intent intent = new Intent(this, RelativeLayoutActivity.class);
        startActivity(intent);
    }

}
